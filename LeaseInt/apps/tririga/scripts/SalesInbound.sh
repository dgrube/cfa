#!/bin/bash

file_name=Tririga_Monthly_Sales_and_Exclusion.txt
current_time=$(date "+%m%d%Y_%H.%M.%S")
new_file_name=$file_name.$current_time


# note that IO is setup up to archive the Inbound file once processed, so there shouldn't be an AP  file in ./data path by the time we get the next one
lftp sftp://tango:fb7bhjcy@files.chick-fil-a.biz -e "get /test/tririga/sales/$file_name -o /apps/tririga/scripts/data/SalesInbound/; mv /dev/tririga/sales/$file_name /dev/tririga/sales/Archive/$new_file_name; bye" 

# 12/20/16 commenting out the below call to execute the IO.  This script will be executed every day to move the Sales
# file from FTP to Tririga.  It will be the responsibility of CFA Lease team to manually execute the IO
# within Tririga following the instructions provided.
# if [ -f /apps/tririga/scripts/data/SalesInbound/$file_name ]
#  then
#    curl -k 'https://leaseint.iwms.chick-fil-a.biz/tririga/html/en/default/rest/Integration?USERNAME=SalesIntegration&PASSWORD=SalesInbound&ioName=cstSalesInboundIO'
# fi

