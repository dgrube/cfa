#!/bin/bash

file_name=GLCompany.csv
current_time=$(date "+%Y.%m.%d-%H.%M.%S")
new_file_name=$file_name.$current_time


# note that IO is setup up to archive the Inbound file once processed, so there shouldn't be a GL file in ./data path by the time we get the next one
lftp sftp://tango:fb7bhjcy@files.chick-fil-a.biz -e "get /test/tririga/gl/companies/data/$file_name -o /apps/tririga/scripts/data/glcompany; mv /test/tririga/gl/companies/data/$file_name /test/tririga/gl/companies/archive/$new_file_name; bye" 

if [ -f /apps/tririga/scripts/data/glcompany/$file_name ]
  then
	  # Remove header row
	  tail -n +2 /apps/tririga/scripts/data/glcompany/$file_name > /apps/tririga/scripts/data/glcompany/$file_name.tmp1
	  # convert from comma delimited to pipe. this handles the quoted description field as well
  	  awk -f /apps/tririga/scripts/Comma2Pipe.awk </apps/tririga/scripts/data/glcompany/$file_name.tmp1 >/apps/tririga/scripts/data/glcompany/$file_name.tmp
	  # remove Enabled_Flag=N records from the file - these are treated as if they did not exist in file and will be retired
	  awk -f /apps/tririga/scripts/RemoveN.awk </apps/tririga/scripts/data/glcompany/$file_name.tmp >/apps/tririga/scripts/data/glcompany/$file_name	  
	  rm /apps/tririga/scripts/data/glcompany/$file_name.tmp
	  rm /apps/tririga/scripts/data/glcompany/$file_name.tmp1
	  # launch the Tririga IO/DT integration
	  curl -k 'https://leaseint.iwms.chick-fil-a.biz/tririga/html/en/default/rest/Integration?USERNAME=GLCompanyIntegration&PASSWORD=GLCompanyInbound&ioName=cstGLCompaniesLauncherIO'
fi

