#!/bin/bash

file_name=APInvoiceRemittance.csv
current_time=$(date "+%m%d%Y_%H.%M.%S")
new_file_name=$file_name.$current_time


# note that IO is setup up to archive the Inbound file once processed, so there shouldn't be an AP  file in ./data path by the time we get the next one
lftp sftp://tango:fb7bhjcy@files.chick-fil-a.biz -e "get /test/tririga/invoices/remittance/data/$file_name -o /apps/tririga/scripts/data/APInbound/; mv /test/tririga/invoices/remittance/data/$file_name /test/tririga/invoices/remittance/archive/$new_file_name; bye" 

if [ -f /apps/tririga/scripts/data/APInbound/$file_name ]
  then
	  curl -k 'https://leaseint.iwms.chick-fil-a.biz/tririga/html/en/default/rest/Integration?USERNAME=APInboundIntegration&PASSWORD=APInbound&ioName=cstAPInboundIO'
fi

