BEGIN{
	FS="\"";
	OFS="";
}
{
	for (i=1;i<=NF;i+=2) gsub(/,/,"|",$i);
	
	print $1, $2, $3;

}